# Googa search engine scraper 

[![build status](https://gitlab.com/serphacker/googa/badges/master/pipeline.svg)](https://gitlab.com/serphacker/googa/commits/master) 
[![coverage report](https://gitlab.com/serphacker/googa/badges/master/coverage.svg)](https://gitlab.com/serphacker/googa/commits/master)
[![code quality](https://api.codacy.com/project/badge/Grade/a1b564ef434c41e08af674bbec05b186)](https://www.codacy.com/app/noguespi/googa?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=serphacker/googa&amp;utm_campaign=Badge_Grade)
[![Maven Central](https://img.shields.io/maven-central/v/com.serphacker.googa/googa.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22com.serphacker.googa%22%20AND%20a:%22googa%22)

Googa is a java search engine scraper. 

Homepage :  https://gitlab.com/serphacker/googa

Issues and bug report : https://gitlab.com/serphacker/googa/issues
 
 ## Install
 
 [![Maven Central](https://img.shields.io/maven-central/v/com.serphacker.googa/googa.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22com.serphacker.googa%22%20AND%20a:%22googa%22)
 (require java minimal version 11 )

 ```xml
<dependency>
  <groupId>com.serphacker.googa</groupId>
  <artifactId>googa</artifactId>
  <version>LATEST</version>
</dependency>
```
 
 ## Usage
 
 TODO
 
 ## Build

### Building jar

`mvn clean package`

## License

The MIT License (MIT)